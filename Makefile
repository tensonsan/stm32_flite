

TARGET = main_m7
MCU_SPEC = cortex-m7
FPU_SPEC = fpv5-d16

TOOLCHAIN = ./gcc-arm-none-eabi-10-2020-q4-major/bin
CC = $(TOOLCHAIN)/arm-none-eabi-gcc
CPP = $(TOOLCHAIN)/arm-none-eabi-g++
AS = $(TOOLCHAIN)/arm-none-eabi-as
LD = $(TOOLCHAIN)/arm-none-eabi-ld
OC = $(TOOLCHAIN)/arm-none-eabi-objcopy
OD = $(TOOLCHAIN)/arm-none-eabi-objdump
OS = $(TOOLCHAIN)/arm-none-eabi-size

# Assembly directives.
ASFLAGS += -mcpu=$(MCU_SPEC)
ASFLAGS += -mthumb

# C compilation directives
CFLAGS += -mcpu=$(MCU_SPEC)
CFLAGS += -mthumb
CFLAGS += -mhard-float
CFLAGS += -mfloat-abi=hard
CFLAGS += -mfpu=$(FPU_SPEC)
CFLAGS += -Wall
CFLAGS += -g
CFLAGS += -O1
CFLAGS += -fmessage-length=0 -fno-common
CFLAGS += -ffunction-sections -fdata-sections
CFLAGS += -std=c99

# C++ compilation directives
CPPFLAGS += -mcpu=$(MCU_SPEC)
CPPFLAGS += -mthumb
CPPFLAGS += -mhard-float
CPPFLAGS += -mfloat-abi=hard
CPPFLAGS += -mfpu=$(FPU_SPEC)
CPPFLAGS += -Wall
CPFLAGS += -g
CPPFLAGS += -O1
CPPFLAGS += -fmessage-length=0 -fno-common
CPPFLAGS += -ffunction-sections -fdata-sections
CPPFLAGS += -fno-exceptions

# Linker directives.
LSCRIPT = ./$(LD_SCRIPT)
LFLAGS += -mcpu=$(MCU_SPEC)
LFLAGS += -mthumb
LFLAGS += -mhard-float
LFLAGS += -mfloat-abi=hard
LFLAGS += -mfpu=$(FPU_SPEC)
LFLAGS += -Wall
LFLAGS += --static
LFLAGS += -Wl,-Map=$(TARGET).map
LFLAGS += -Wl,--gc-sections
LFLAGS += -lgcc
LFLAGS += -lc
LFLAGS += -T$(LSCRIPT)
LFLAGS += -Wl,--print-memory-usage

C_SRC += ./CM7/Src/main.c
C_SRC += ./CM7/Src/audio_play.c
C_SRC += ./CM7/Src/fifo.c
C_SRC += ./CM7/Src/stm32h7xx_hal_msp.c
C_SRC += ./CM7/Src/stm32h7xx_it.c
C_SRC += ./CM7/Src/stm32h7xx_hal_timebase_tim.c

INCLUDE += -I./CM7/Inc

DEFS += -DCORE_CM7

LD_SCRIPT = ./CM7/STM32H745XIHx_FLASH_CM7.ld

# Source files common
AS_SRC = ./Common/startup_stm32h745xx.s

C_SRC += ./Common/Src/system_stm32h7xx.c
C_SRC += ./Common/Src/Retarget.c

C_SRC += ./Drivers/STM32H7xx_HAL_Driver/Src/stm32h7xx_hal.c
C_SRC += ./Drivers/STM32H7xx_HAL_Driver/Src/stm32h7xx_hal_rcc.c
C_SRC += ./Drivers/STM32H7xx_HAL_Driver/Src/stm32h7xx_hal_rcc_ex.c
C_SRC += ./Drivers/STM32H7xx_HAL_Driver/Src/stm32h7xx_hal_cortex.c
C_SRC += ./Drivers/STM32H7xx_HAL_Driver/Src/stm32h7xx_hal_pwr.c
C_SRC += ./Drivers/STM32H7xx_HAL_Driver/Src/stm32h7xx_hal_pwr_ex.c
C_SRC += ./Drivers/STM32H7xx_HAL_Driver/Src/stm32h7xx_hal_gpio.c
C_SRC += ./Drivers/STM32H7xx_HAL_Driver/Src/stm32h7xx_hal_sdram.c
C_SRC += ./Drivers/STM32H7xx_HAL_Driver/Src/stm32h7xx_hal_qspi.c
C_SRC += ./Drivers/STM32H7xx_HAL_Driver/Src/stm32h7xx_hal_mdma.c
C_SRC += ./Drivers/STM32H7xx_HAL_Driver/Src/stm32h7xx_hal_uart.c
C_SRC += ./Drivers/STM32H7xx_HAL_Driver/Src/stm32h7xx_hal_tim.c
C_SRC += ./Drivers/STM32H7xx_HAL_Driver/Src/stm32h7xx_hal_tim_ex.c
C_SRC += ./Drivers/STM32H7xx_HAL_Driver/Src/stm32h7xx_hal_sai.c
C_SRC += ./Drivers/STM32H7xx_HAL_Driver/Src/stm32h7xx_hal_dma.c
C_SRC += ./Drivers/STM32H7xx_HAL_Driver/Src/stm32h7xx_hal_i2c.c
C_SRC += ./Drivers/STM32H7xx_HAL_Driver/Src/stm32h7xx_hal_i2c_ex.c
C_SRC += ./Drivers/STM32H7xx_HAL_Driver/Src/stm32h7xx_ll_fmc.c

C_SRC += ./Drivers/BSP/STM32H745I-DISCO/stm32h745i_discovery.c
C_SRC += ./Drivers/BSP/STM32H745I-DISCO/stm32h745i_discovery_qspi.c
C_SRC += ./Drivers/BSP/STM32H745I-DISCO/stm32h745i_discovery_sdram.c
C_SRC += ./Drivers/BSP/STM32H745I-DISCO/stm32h745i_discovery_audio.c
C_SRC += ./Drivers/BSP/STM32H745I-DISCO/stm32h745i_discovery_bus.c
C_SRC += ./Drivers/BSP/Components/mt48lc4m32b2/mt48lc4m32b2.c
C_SRC += ./Drivers/BSP/Components/mt25tl01g/mt25tl01g.c
C_SRC += ./Drivers/BSP/Components/wm8994/wm8994.c
C_SRC += ./Drivers/BSP/Components/wm8994/wm8994_reg.c

C_SRC += ./FreeRTOS/Source/croutine.c
C_SRC += ./FreeRTOS/Source/event_groups.c
C_SRC += ./FreeRTOS/Source/list.c
C_SRC += ./FreeRTOS/Source/queue.c
C_SRC += ./FreeRTOS/Source/stream_buffer.c
C_SRC += ./FreeRTOS/Source/tasks.c
C_SRC += ./FreeRTOS/Source/timers.c
C_SRC += ./FreeRTOS/Source/portable/MemMang/heap_4.c
C_SRC += ./FreeRTOS/Source/portable/GCC/ARM_CM7/r0p1/port.c

C_SRC += ./flite/src/regex/cst_regex.c
C_SRC += ./flite/src/regex/regexp.c

C_SRC += ./flite/src/synth/flite.c
C_SRC += ./flite/src/synth/cst_synth.c
C_SRC += ./flite/src/synth/cst_utt_utils.c
C_SRC += ./flite/src/synth/cst_voice.c
C_SRC += ./flite/src/synth/cst_phoneset.c
C_SRC += ./flite/src/synth/cst_ffeatures.c

C_SRC += ./flite/src/lexicon/cst_lexicon.c
C_SRC += ./flite/src/lexicon/cst_lts.c

C_SRC += ./flite/src/wavesynth/cst_diphone.c
C_SRC += ./flite/src/wavesynth/cst_clunits.c
C_SRC += ./flite/src/wavesynth/cst_sts.c
C_SRC += ./flite/src/wavesynth/cst_units.c
C_SRC += ./flite/src/wavesynth/cst_sigpr.c

C_SRC += ./flite/src/speech/cst_wave.c
C_SRC += ./flite/src/speech/cst_wave_io.c
C_SRC += ./flite/src/speech/cst_lpcres.c
C_SRC += ./flite/src/speech/cst_wave_utils.c
C_SRC += ./flite/src/speech/g72x.c
C_SRC += ./flite/src/speech/g721.c
C_SRC += ./flite/src/speech/cst_track.c
C_SRC += ./flite/src/speech/rateconv.c

C_SRC += ./flite/src/cg/cst_cg.c
C_SRC += ./flite/src/cg/cst_spamf0.c
C_SRC += ./flite/src/cg/cst_mlpg.c
C_SRC += ./flite/src/cg/cst_vc.c
C_SRC += ./flite/src/cg/cst_mlsa.c

C_SRC += ./flite/src/stats/cst_cart.c

C_SRC += ./flite/src/hrg/cst_utterance.c
C_SRC += ./flite/src/hrg/cst_relation.c
C_SRC += ./flite/src/hrg/cst_item.c
C_SRC += ./flite/src/hrg/cst_ffeature.c

C_SRC += ./flite/src/audio/au_streaming.c
C_SRC += ./flite/src/audio/audio.c
C_SRC += ./flite/src/audio/au_none.c

C_SRC += ./flite/lang/cmu_us_kal16/cmu_us_kal16.c
C_SRC += ./flite/lang/cmu_us_kal16/cmu_us_kal16_diphone.c
C_SRC += ./flite/lang/cmu_us_kal16/cmu_us_kal16_lpc.c
C_SRC += ./flite/lang/cmu_us_kal16/cmu_us_kal16_res.c
C_SRC += ./flite/lang/cmu_us_kal16/cmu_us_kal16_residx.c

C_SRC += ./flite/lang/cmu_us_slt/cmu_us_slt_cg_durmodel.c
C_SRC += ./flite/lang/cmu_us_slt/cmu_us_slt_cg_f0_trees.c
C_SRC += ./flite/lang/cmu_us_slt/cmu_us_slt_cg_phonestate.c
C_SRC += ./flite/lang/cmu_us_slt/cmu_us_slt_cg_single_mcep_trees.c
C_SRC += ./flite/lang/cmu_us_slt/cmu_us_slt_cg_single_params.c
C_SRC += ./flite/lang/cmu_us_slt/cmu_us_slt_cg.c
C_SRC += ./flite/lang/cmu_us_slt/cmu_us_slt_spamf0_accent_params.c
C_SRC += ./flite/lang/cmu_us_slt/cmu_us_slt_spamf0_accent.c
C_SRC += ./flite/lang/cmu_us_slt/cmu_us_slt_spamf0_phrase.c
C_SRC += ./flite/lang/cmu_us_slt/cmu_us_slt.c

C_SRC += ./flite/lang/cmu_us_rms/cmu_us_rms_cg_durmodel.c
C_SRC += ./flite/lang/cmu_us_rms/cmu_us_rms_cg_f0_trees.c
C_SRC += ./flite/lang/cmu_us_rms/cmu_us_rms_cg_phonestate.c
C_SRC += ./flite/lang/cmu_us_rms/cmu_us_rms_cg_single_mcep_trees.c
C_SRC += ./flite/lang/cmu_us_rms/cmu_us_rms_cg_single_params.c
C_SRC += ./flite/lang/cmu_us_rms/cmu_us_rms_cg.c
C_SRC += ./flite/lang/cmu_us_rms/cmu_us_rms_spamf0_accent_params.c
C_SRC += ./flite/lang/cmu_us_rms/cmu_us_rms_spamf0_accent.c
C_SRC += ./flite/lang/cmu_us_rms/cmu_us_rms_spamf0_phrase.c
C_SRC += ./flite/lang/cmu_us_rms/cmu_us_rms.c

C_SRC += ./flite/lang/usenglish/usenglish.c
C_SRC += ./flite/lang/usenglish/us_phoneset.c
C_SRC += ./flite/lang/usenglish/us_text.c
C_SRC += ./flite/lang/usenglish/us_pos_cart.c
C_SRC += ./flite/lang/usenglish/us_phrasing_cart.c
C_SRC += ./flite/lang/usenglish/us_int_accent_cart.c
C_SRC += ./flite/lang/usenglish/us_int_tone_cart.c
C_SRC += ./flite/lang/usenglish/us_durz_cart.c
C_SRC += ./flite/lang/usenglish/us_dur_stats.c
C_SRC += ./flite/lang/usenglish/us_expand.c
C_SRC += ./flite/lang/usenglish/us_f0_model.c
C_SRC += ./flite/lang/usenglish/us_ffeatures.c
C_SRC += ./flite/lang/usenglish/us_nums_cart.c
C_SRC += ./flite/lang/usenglish/us_aswd.c
C_SRC += ./flite/lang/usenglish/us_f0lr.c
C_SRC += ./flite/lang/usenglish/us_gpos.c

C_SRC += ./flite/lang/cmulex/cmu_lex.c
C_SRC += ./flite/lang/cmulex/cmu_postlex.c
C_SRC += ./flite/lang/cmulex/cmu_lex_entries.c
C_SRC += ./flite/lang/cmulex/cmu_lex_data.c
C_SRC += ./flite/lang/cmulex/cmu_lts_rules.c
C_SRC += ./flite/lang/cmulex/cmu_lts_model.c

C_SRC += ./flite/src/utils/cst_features.c
C_SRC += ./flite/src/utils/cst_tokenstream.c
C_SRC += ./flite/src/utils/cst_val.c
C_SRC += ./flite/src/utils/cst_string.c
C_SRC += ./flite/src/utils/cst_error.c
C_SRC += ./flite/src/utils/cst_file_stdio.c
C_SRC += ./flite/src/utils/cst_alloc.c
C_SRC += ./flite/src/utils/cst_val_user.c
C_SRC += ./flite/src/utils/cst_mmap_none.c
C_SRC += ./flite/src/utils/cst_endian.c
C_SRC += ./flite/src/utils/cst_val_const.c
C_SRC += ./flite/src/utils/cst_url.c
C_SRC += ./flite/src/utils/cst_socket.c

INCLUDE += -I./flite/include
INCLUDE += -I./flite/lang/cmu_us_kal16
INCLUDE += -I./flite/lang/cmu_us_slt
INCLUDE += -I./flite/lang/cmu_us_rms
INCLUDE += -I./flite/lang/cmulex
INCLUDE += -I./flite/lang/usenglish

INCLUDE += -I./Common/Inc

INCLUDE += -I./Drivers
INCLUDE += -I./Drivers/CMSIS/Include
INCLUDE += -I./Drivers/CMSIS/Device/ST/STM32H7xx/Include

INCLUDE += -I./Drivers/STM32H7xx_HAL_Driver/Inc

INCLUDE += -I./Drivers/BSP/STM32H745I-DISCO
INCLUDE += -I./Drivers/BSP/Components/mt48lc4m32b2
INCLUDE += -I./Drivers/BSP/Components/mt25tl01g
INCLUDE += -I./Drivers/BSP/Components/wm8994

INCLUDE += -I./FreeRTOS/Source/include
INCLUDE += -I./FreeRTOS/Source/portable/GCC/ARM_CM7/r0p1

DEFS += -DSTM32H745xx -DUSE_HAL_DRIVER -DUSE_BSP_COM_FEATURE -DCST_NO_SOCKETS

OBJS = $(C_SRC:.c=.o)
OBJS += $(CPP_SRC:.cpp=.o)
OBJS += $(AS_SRC:.s=.o)

.PHONY: all
all: $(TARGET).bin $(TARGET).hex

%.o: %.s
	$(AS) $(ASFLAGS) -c $< -o $@

%.o: %.c
	$(CC) -c $(CFLAGS) $(INCLUDE) $(DEFS) $< -o $@

%.o: %.cpp
	$(CPP) -c $(CPPFLAGS) $(INCLUDE) $(DEFS) $< -o $@

$(TARGET).elf: $(OBJS)
	$(CPP) $^ $(LFLAGS) -o $@

$(TARGET).bin: $(TARGET).elf
	$(OC) -S -O binary $< $@
	$(OS) $<

$(TARGET).hex: $(TARGET).elf
	$(OC) -S -O ihex $< $@
	$(OS) $<

.PHONY: clean
clean:
	rm -f $(OBJS)
	rm -f $(TARGET).elf
	rm -f $(TARGET).bin
	rm -f $(TARGET).hex
	rm -f $(TARGET).map