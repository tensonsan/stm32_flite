
#ifndef _FIFO_H_
#define _FIFO_H_

#include <stdint.h>

#define FIFO_BUFFER_SIZE (8 * 2048)

struct fifo
{
    uint16_t buffer[FIFO_BUFFER_SIZE];
    uint32_t wr;
    uint32_t rd;
    uint32_t count;
};

void fifo_init(struct fifo *f);
int32_t fifo_write(struct fifo *f, uint16_t data);
int32_t fifo_read(struct fifo *f, uint16_t *data);
uint32_t fifo_count(struct fifo *f);

#endif
