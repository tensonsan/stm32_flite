
#include "fifo.h"
#include "stm32h7xx_hal.h"

void fifo_init(struct fifo *f)
{
    f->wr = 0;
    f->rd = 0;
    f->count = 0;
}

int32_t fifo_write(struct fifo *f, uint16_t data)
{
    int32_t rv = -1;

    __disable_irq();

    if (f->count < FIFO_BUFFER_SIZE)
    {
        f->buffer[f->wr++] = data;
        if (f->wr >= FIFO_BUFFER_SIZE)
        {
            f->wr = 0;
        }
        f->count++;

        rv = 0;
    }

    __enable_irq();

    return rv;
}

int32_t fifo_read(struct fifo *f, uint16_t *data)
{
    int32_t rv = -1;

    __disable_irq();

    if (f->count > 0)
    {
        *data = f->buffer[f->rd++];
        if (f->rd >= FIFO_BUFFER_SIZE)
        {
            f->rd = 0;
        }
        f->count--;

        rv = 0;
    }

    __enable_irq();

    return rv;
}

uint32_t fifo_count(struct fifo *f)
{
    return f->count;
}
