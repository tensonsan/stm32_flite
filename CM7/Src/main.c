
#include <stdio.h>
#include <stdint.h>
#include "FreeRTOS.h"
#include "flite.h"
#include "task.h"
#include "main.h"
#include "stm32h745i_discovery.h"
#include "stm32h745i_discovery_qspi.h"
#include "stm32h745i_discovery_sdram.h"
#include "stm32h7xx_hal.h"
#include "audio_play.h"

#define TEXT_BUFFER_SIZE  128
#define NUM_TASKS 5

uint8_t __attribute__((section(".ExtSDRAMSection")))ucHeap[configTOTAL_HEAP_SIZE];

static void CPU_CACHE_Enable(void);
static void SystemClock_Config(void);
static void Error_Handler(void);

static void lcd_backlight_off(void)
{
    __HAL_RCC_GPIOK_CLK_ENABLE();

    GPIO_InitTypeDef  gpio_init;
    gpio_init.Mode = GPIO_MODE_OUTPUT_PP;
    gpio_init.Pull = GPIO_NOPULL;
    gpio_init.Speed = GPIO_SPEED_FREQ_LOW;
    gpio_init.Pin = GPIO_PIN_0;

    HAL_GPIO_Init(GPIOK, &gpio_init);

    HAL_GPIO_WritePin(GPIOK, GPIO_PIN_0, GPIO_PIN_RESET);
}

/**
  * @brief  Configure the MPU attributes as Write Through for SDRAM.
  * @note   The Base Address is SDRAM_DEVICE_ADDR.
  *         The Region Size is 32MB.
  * @param  None
  * @retval None
  */
static void MPU_Config(void)
{
    MPU_Region_InitTypeDef MPU_InitStruct;

    HAL_MPU_Disable();

    MPU_InitStruct.Enable = MPU_REGION_ENABLE;
    MPU_InitStruct.BaseAddress = SDRAM_DEVICE_ADDR;
    MPU_InitStruct.Size = MPU_REGION_SIZE_32MB;
    MPU_InitStruct.AccessPermission = MPU_REGION_FULL_ACCESS;
    MPU_InitStruct.IsBufferable = MPU_ACCESS_NOT_BUFFERABLE;
    MPU_InitStruct.IsCacheable = MPU_ACCESS_CACHEABLE;
    MPU_InitStruct.IsShareable = MPU_ACCESS_NOT_SHAREABLE;
    MPU_InitStruct.Number = MPU_REGION_NUMBER0;
    MPU_InitStruct.TypeExtField = MPU_TEX_LEVEL0;
    MPU_InitStruct.SubRegionDisable = 0x00;
    MPU_InitStruct.DisableExec = MPU_INSTRUCTION_ACCESS_ENABLE;

    HAL_MPU_ConfigRegion(&MPU_InitStruct);

    HAL_MPU_Enable(MPU_PRIVILEGED_DEFAULT);
}

#if 0
static void clock_info(void)
{
    printf("SYSCLK: %lu Hz\r\n", HAL_RCC_GetSysClockFreq());
    printf("HCLK: %lu Hz\r\n", HAL_RCC_GetSysClockFreq());
    printf("PCLK1: %lu Hz\r\n", HAL_RCC_GetPCLK1Freq());
    printf("PCLK2: %lu Hz\r\n", HAL_RCC_GetPCLK2Freq());
    printf("D1PCLK1: %lu Hz\r\n", HAL_RCCEx_GetD1PCLK1Freq());
    printf("D3PCLK1: %lu Hz\r\n", HAL_RCCEx_GetD3PCLK1Freq());
    printf("D1SYSCLK: %lu Hz\r\n", HAL_RCCEx_GetD1SysClockFreq());
}

static void stack_info(void)
{
    printf("Free heap: %u\r\n", xPortGetFreeHeapSize());
    printf("Min free heap: %u\r\n", xPortGetMinimumEverFreeHeapSize());
}

static void task_info(void)
{
    char buffer[40*NUM_TASKS];

    vTaskList(buffer);
    printf("%s\r\n", buffer);
}
#endif

//extern cst_voice *register_cmu_us_slt(const char *voxdir);
extern cst_voice *register_cmu_us_rms(const char *voxdir);

static int my_audio_stream_chunk(const cst_wave *w, int start, int size, int last, cst_audio_streaming_info *asi)
{
    // We need stereo samples for the HAL BSP audio API to work
    for (int32_t i=0; i < size; i++)
    {
        uint16_t data = w->samples[i + start];

        audio_play_write(data);
        audio_play_write(data);
    }

    // Padding for the last buffer
    if (last)
    {
        int32_t sz =  audio_play_padding();
        for (int32_t i=0; i < sz; ++i)
        {
            audio_play_write(0);
        }
    }

    return CST_AUDIO_STREAM_CONT;
}

void main_task(void *p)
{
    //clock_info();
    //stack_info();
    //task_info();
    cst_voice *v = NULL;

    int32_t rv = flite_init();
    printf("flite: %ld\r\n", rv);
    if (0 == rv)
    {
        rv = -1;

        //v = register_cmu_us_slt(NULL);
        v = register_cmu_us_rms(NULL);
        if (v)
        {
            cst_audio_streaming_info *asi = new_audio_streaming_info();
            if (asi)
            {
                asi->asc = my_audio_stream_chunk;
                feat_set(v->features, "streaming_info", audio_streaming_info_val(asi));
                rv = 0;
            }
        }
    }

    char text_buffer[TEXT_BUFFER_SIZE];
    uint32_t text_count = 0;

    while (1)
    {
        int c = getchar();
        if (c == '\r')
        {
            text_buffer[text_count] = '\0';
            if (text_count > 0)
            {
                audio_play_prepare();

                if (0 == rv)
                {
                    flite_text_to_speech(text_buffer, v, "stream");
                }
                else
                {
                    printf("\r\nTTS conversion failed\r\n");
                }

                //stack_info();
                //task_info();

                text_count = 0;
            }
            printf("\r\n");
        }
        else
        {
            if (text_count >= TEXT_BUFFER_SIZE)
            {
                printf("\r\nString too long\r\n");
                text_count = 0;
            }
            text_buffer[text_count++] = (char)c;
            printf("%c", c);
        }
    }
}

void audio_stream_task(void *p)
{
    int32_t rv = audio_play_init(16000, 70);
    printf("audio: %ld\r\n", rv);

    while (1)
    {
        audio_play_process();
    }
}

void led_task(void *p)
{
    while (1)
    {
        BSP_LED_Toggle(LED_GREEN);
        vTaskDelay(500);
    }
}

int main(void)
{
    int32_t rv;
    setvbuf(stdin, NULL, _IONBF, 0);
    setvbuf(stdout, NULL, _IONBF, 0);
    setvbuf(stderr, NULL, _IONBF, 0);

    lcd_backlight_off();

    BSP_LED_Init(LED_GREEN);
    BSP_LED_Init(LED_RED);

    MPU_Config();

    CPU_CACHE_Enable();

    HAL_Init();

    SystemClock_Config();

    COM_InitTypeDef com_init;
    com_init.BaudRate   = 115200;
    com_init.WordLength = COM_WORDLENGTH_8B;
    com_init.StopBits   = COM_STOPBITS_1;
    com_init.Parity     = COM_PARITY_NONE;
    com_init.HwFlowCtl  = COM_HWCONTROL_NONE;
    BSP_COM_Init(COM1, &com_init);

    BSP_QSPI_Init_t qspi_init;
    qspi_init.InterfaceMode = MT25TL01G_QPI_MODE;
    qspi_init.TransferRate = MT25TL01G_DTR_TRANSFER;
    qspi_init.DualFlashMode = MT25TL01G_DUALFLASH_ENABLE;

    rv = BSP_QSPI_Init(0, &qspi_init);
    printf("qspi: %ld\r\n", rv);

    rv = BSP_QSPI_EnableMemoryMappedMode(0);
    printf("qspi mm: %ld\r\n", rv);

    rv = BSP_SDRAM_Init(0);
    printf("sdram: %ld\r\n", rv);

    xTaskCreate(main_task, "main", 2048, 0, 1, 0);
    xTaskCreate(led_task, "led", 512, 0, 1, 0);
    xTaskCreate(audio_stream_task, "audio", 1024, 0, 1, 0);

    vTaskStartScheduler();

    BSP_LED_On(LED_RED);

    while (1)
    {
    }
}

/**
  * @brief  System Clock Configuration
  *         The system Clock is configured as follow : 
  *            System Clock source            = PLL (HSE)
  *            SYSCLK(Hz)                     = 400000000 (Cortex-M7 CPU Clock)
  *            HCLK(Hz)                       = 200000000 (Cortex-M4 CPU, Bus matrix Clocks)
  *            AHB Prescaler                  = 2
  *            D1 APB3 Prescaler              = 2 (APB3 Clock  100MHz)
  *            D2 APB1 Prescaler              = 2 (APB1 Clock  100MHz)
  *            D2 APB2 Prescaler              = 2 (APB2 Clock  100MHz)
  *            D3 APB4 Prescaler              = 2 (APB4 Clock  100MHz)
  *            HSE Frequency(Hz)              = 25000000
  *            PLL_M                          = 5
  *            PLL_N                          = 160
  *            PLL_P                          = 2
  *            PLL_Q                          = 4
  *            PLL_R                          = 2
  *            VDD(V)                         = 3.3
  *            Flash Latency(WS)              = 4
  * @param  None
  * @retval None
  */
static void SystemClock_Config(void)
{
    RCC_ClkInitTypeDef RCC_ClkInitStruct;
    RCC_OscInitTypeDef RCC_OscInitStruct;
    HAL_StatusTypeDef ret = HAL_OK;

    /*!< Supply configuration update enable */
    HAL_PWREx_ConfigSupply(PWR_DIRECT_SMPS_SUPPLY);

    /* The voltage scaling allows optimizing the power consumption when the device is 
        clocked below the maximum system frequency, to update the voltage scaling value 
        regarding system frequency refer to product datasheet.  */
    __HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE1);

    while(!__HAL_PWR_GET_FLAG(PWR_FLAG_VOSRDY)) {}

    /* Enable HSE Oscillator and activate PLL with HSE as source */
    RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSE;
    RCC_OscInitStruct.HSEState = RCC_HSE_ON;
    RCC_OscInitStruct.HSIState = RCC_HSI_OFF;
    RCC_OscInitStruct.CSIState = RCC_CSI_OFF;
    RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
    RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;

    RCC_OscInitStruct.PLL.PLLM = 5;
    RCC_OscInitStruct.PLL.PLLN = 160;
    RCC_OscInitStruct.PLL.PLLFRACN = 0;
    RCC_OscInitStruct.PLL.PLLP = 2;
    RCC_OscInitStruct.PLL.PLLR = 2;
    RCC_OscInitStruct.PLL.PLLQ = 4;

    RCC_OscInitStruct.PLL.PLLVCOSEL = RCC_PLL1VCOWIDE;
    RCC_OscInitStruct.PLL.PLLRGE = RCC_PLL1VCIRANGE_2;
    ret = HAL_RCC_OscConfig(&RCC_OscInitStruct);
    if(ret != HAL_OK)
    {
        Error_Handler();
    }

    /* Select PLL as system clock source and configure  bus clocks dividers */
    RCC_ClkInitStruct.ClockType = (RCC_CLOCKTYPE_SYSCLK | RCC_CLOCKTYPE_HCLK | RCC_CLOCKTYPE_D1PCLK1 | RCC_CLOCKTYPE_PCLK1 | \
                                    RCC_CLOCKTYPE_PCLK2  | RCC_CLOCKTYPE_D3PCLK1);

    RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
    RCC_ClkInitStruct.SYSCLKDivider = RCC_SYSCLK_DIV1;
    RCC_ClkInitStruct.AHBCLKDivider = RCC_HCLK_DIV2;
    RCC_ClkInitStruct.APB3CLKDivider = RCC_APB3_DIV2;  
    RCC_ClkInitStruct.APB1CLKDivider = RCC_APB1_DIV2; 
    RCC_ClkInitStruct.APB2CLKDivider = RCC_APB2_DIV2; 
    RCC_ClkInitStruct.APB4CLKDivider = RCC_APB4_DIV2; 
    ret = HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_4);
    if(ret != HAL_OK)
    {
        Error_Handler();
    }

    /*
    Note : The activation of the I/O Compensation Cell is recommended with communication  interfaces
            (GPIO, SPI, FMC, QSPI ...)  when  operating at  high frequencies(please refer to product datasheet)       
            The I/O Compensation Cell activation  procedure requires :
        - The activation of the CSI clock
        - The activation of the SYSCFG clock
        - Enabling the I/O Compensation Cell : setting bit[0] of register SYSCFG_CCCSR

            To do this please uncomment the following code 
    */
    __HAL_RCC_CSI_ENABLE();

    __HAL_RCC_SYSCFG_CLK_ENABLE();

    HAL_EnableCompensationCell();
}

/**
  * @brief  This function is executed in case of error occurrence.
  * @param  None
  * @retval None
  */
static void Error_Handler(void)
{
    BSP_LED_On(LED_RED);

    while(1)
    {
    }
}

/**
  * @brief  CPU L1-Cache enable.
  * @param  None
  * @retval None
  */
static void CPU_CACHE_Enable(void)
{
    SCB_EnableICache();
    SCB_EnableDCache();
}


#ifdef  USE_FULL_ASSERT

/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t* file, uint32_t line)
{ 
    BSP_LED_On(LED_RED);

    while (1)
    {
    }
}
#endif
