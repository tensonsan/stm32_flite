
#ifndef _AUDIO_PLAY_H_
#define _AUDIO_PLAY_H_

#include <stdint.h>

int32_t audio_play_init(uint32_t samplerate, uint32_t volume);
void audio_play_prepare(void);
void audio_play_process(void);
void audio_play_write(uint16_t data);
int32_t audio_play_padding(void);

#endif
