
#include <stdio.h>
#include "audio_play.h"
#include "fifo.h"
#include "stm32h745i_discovery_audio.h"
#include "FreeRTOS.h"
#include "task.h"

#define AUDIO_BUFFER_SIZE 2048

enum audio_state_e
{
    AUDIO_STATE_IDLE = 0,
    AUDIO_STATE_INIT,
    AUDIO_STATE_HALF,
    AUDIO_STATE_FULL,
    AUDIO_STATE_ERROR,
    AUDIO_STATE_DONE
};

static uint16_t audio_buffer[AUDIO_BUFFER_SIZE] __attribute__((aligned(32)));
static enum audio_state_e audio_state = AUDIO_STATE_IDLE;
static enum audio_state_e audio_state_prev = AUDIO_STATE_IDLE;
struct fifo audio_fifo;
static int32_t audio_count = 0;

/**
  * @brief  Manages the full Transfer complete event.
  * @param  None
  * @retval None
  */
void BSP_AUDIO_OUT_TransferComplete_CallBack(uint32_t Interface)
{
    audio_state = AUDIO_STATE_FULL;
}

/**
  * @brief  Manages the DMA Half Transfer complete event.
  * @param  None
  * @retval None
  */
void BSP_AUDIO_OUT_HalfTransfer_CallBack(uint32_t Interface)
{
    audio_state = AUDIO_STATE_HALF;
}

/**
  * @brief  Manages the DMA FIFO error event.
  * @param  None
  * @retval None
  */
void BSP_AUDIO_OUT_Error_CallBack(uint32_t Interface)
{
    audio_state = AUDIO_STATE_ERROR;
}

int32_t audio_play_padding(void)
{
    return ((~audio_count + 1) & (AUDIO_BUFFER_SIZE - 1));
}

void audio_play_write(uint16_t data)
{
    while (1)
    {
        int32_t rv = fifo_write(&audio_fifo, data);
        if (!rv)
        {
            break;
        }
        else
        {
            taskYIELD();
        }
    }

    audio_count++;
}

void audio_play_process(void)
{
    int32_t i, rv;

    switch (audio_state)
    {
    case AUDIO_STATE_IDLE:
        taskYIELD();
        break;

    case AUDIO_STATE_INIT:
        // DMA empty: fill the entire streaming buffer
        if (fifo_count(&audio_fifo) >= AUDIO_BUFFER_SIZE)
        {
            for (i=0; i < AUDIO_BUFFER_SIZE; ++i)
            {
                fifo_read(&audio_fifo, &audio_buffer[i]);
            }

            SCB_CleanDCache_by_Addr((uint32_t *)&audio_buffer[0], AUDIO_BUFFER_SIZE * sizeof(uint16_t));
            rv = BSP_AUDIO_OUT_Play(0, (uint8_t *)&audio_buffer[0], AUDIO_BUFFER_SIZE * sizeof(uint16_t));
            printf("\r\naudio play %ld\r\n", rv);

            audio_state = AUDIO_STATE_IDLE;
        }
        break;

    case AUDIO_STATE_HALF:
        // DMA half-full complete: second half of the buffer will start streaming, fill the first half of the buffer
        if (fifo_count(&audio_fifo) >= (AUDIO_BUFFER_SIZE / 2))
        {
            for (i=0; i < (AUDIO_BUFFER_SIZE / 2); ++i)
            {
                fifo_read(&audio_fifo, &audio_buffer[i]);
            }

            SCB_CleanDCache_by_Addr((uint32_t *)&audio_buffer[0], (AUDIO_BUFFER_SIZE / 2) * sizeof(uint16_t));
        }
        else
        {
            BSP_AUDIO_OUT_Stop(0);
        }
        audio_state = AUDIO_STATE_IDLE;
        break;

    case AUDIO_STATE_FULL:
        // DMA full complete: first half of the buffer will start streaming, fill the second half of the buffer
        if (fifo_count(&audio_fifo) >= (AUDIO_BUFFER_SIZE / 2))
        {
            for (i=0; i < (AUDIO_BUFFER_SIZE / 2); ++i)
            {
                fifo_read(&audio_fifo, &audio_buffer[i + (AUDIO_BUFFER_SIZE / 2)]);
            }

            SCB_CleanDCache_by_Addr((uint32_t *)&audio_buffer[AUDIO_BUFFER_SIZE / 2], (AUDIO_BUFFER_SIZE / 2) * sizeof(uint16_t));
        }
        else
        {
            BSP_AUDIO_OUT_Stop(0);
        }
        audio_state = AUDIO_STATE_IDLE;
        break;

    case AUDIO_STATE_ERROR:
        printf("\r\naudio stream error\r\n");
        BSP_AUDIO_OUT_Stop(0);
        audio_state = AUDIO_STATE_IDLE;
        break;

    default:
        audio_state = AUDIO_STATE_IDLE;
        break;
    }

    if (audio_state_prev != audio_state)
    {
        //printf("\r\nstate: %lu\r\n", audio_state);
        audio_state_prev = audio_state;
    }
}

void audio_play_prepare(void)
{
    // Wait for the previous TTS conversion to finish
    while (AUDIO_STATE_IDLE != audio_state)
    {
        taskYIELD();
    }

    // Trigger new TTS conversion
    audio_count = 0;
    fifo_init(&audio_fifo);
    audio_state = AUDIO_STATE_INIT;
}

int32_t audio_play_init(uint32_t samplerate, uint32_t volume)
{
    fifo_init(&audio_fifo);

    BSP_AUDIO_Init_t audio_init;
    audio_init.Device = AUDIO_OUT_DEVICE_HEADPHONE;
    audio_init.SampleRate = samplerate;
    audio_init.BitsPerSample = AUDIO_RESOLUTION_16B;
    audio_init.ChannelsNbr = 2;
    audio_init.Volume = volume;

    return BSP_AUDIO_OUT_Init(0, &audio_init);
}
